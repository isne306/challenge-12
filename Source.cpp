#include<iostream>
#include<cstring>
#include "list.h"
using namespace std;

template <typename T>
void swap_values(T& variable1, T& variable2);

//void swap_values(char& variable1, char& variable2);

template <typename T>
void sort(T a[], int number_used);

template <typename T>
int index_of_smallest(const T a[], int start_index, int number_used);

int main(){
	char str[] = { 'c', 'h', 'k', 'z', 'a', 'y', 'b' }; //original str[]
	int n[] = { 93, 56, 17, 32, 57, 20, 8 }; //original n[]
	cout << "Challenge 12" << endl;
	cout << endl;

	cout << "unsort string : ";
	for (int i = 0; i < sizeof(str); i++)
	{
		cout  <<str[i] << " ";
	}
	cout << endl;
	sort(str, sizeof(str)); // 1 bit
	cout << "sort string : ";
	for (int i = 0; i < sizeof(str); i++)
	{
		cout  <<str[i] << " ";
	}
	cout << endl;
	cout << endl;

	cout << "unsort number : ";
	for (int i = 0; i < sizeof(n) / 4; i++)
	{
		cout << n[i] << " ";
	}
	cout << endl;

	sort(n, sizeof(n) / 4); // 4 bit

	cout << "sort number : ";
	for (int i = 0; i < sizeof(n) / 4; i++)
	{
		cout << n[i] << " ";
	}
	cout << endl;
	cout << endl;

	List p;

	for (int i = 1; i <= 5; i++)
	{	
		p.headPush(i);
	}
	for (int i = 1; i <= 3; i++)
	{
		p.tailPush(i);
		p.tailPush(1);
		p.headPush(3);
	}

	cout << "Display : ";
	p.display();
	p.sort();
	cout << endl;

	cout << "Sorted : ";
	p.display();
	p.unique();
	cout << endl;

	cout << "After use unique function : ";
	p.display();

}

template <typename T>
void swap_values(T& variable1, T& variable2)
{
	T temp;
	temp = variable1;
	variable1 = variable2;
	variable2 = temp;
}

template <typename T>
void sort(T a[], int number_used)
{
	int index_of_next_smallest;
	for (int index = 0; index < number_used - 1; index++)
	{
		index_of_next_smallest = index_of_smallest(a, index, number_used);
		swap_values(a[index], a[index_of_next_smallest]);
	}
}

template <typename T>
int index_of_smallest(const T a[], int start_index, int number_used)
{
	int min = a[start_index];
	int index_of_min = start_index;
	for (int index = start_index + 1; index < number_used; index++)
		if (a[index] < min)
		{
			min = a[index];
			index_of_min = index;
		}
	return index_of_min;
}