﻿#include <iostream>
#include "list.h"
using namespace std;

List::~List() 
{
	for (Node *p; !isEmpty();)
	{
		p = head->next;
		delete head;
		head = p;
	}
}

void List::display()
{
	Node *p;
	if (isEmpty())
	{cout << "Empty Data" << endl;}
	else
	{
		for (p = head; p != NULL; p = p->next)
		{
			cout << p->info << " ";
		}
		cout << endl;
	}
}

void List::prevshow()
{
	Node *p;
	if (isEmpty())
	{
		cout << "Empty Data" << endl;
	}
	else
	{
		for (p = tail; p != NULL; p = p->prev)
		{
			cout << p->info << " ";
		}
		cout << endl;
	}
}

void List::headPush(int a)
{
	Node *p = new Node(a);
	if (isEmpty())
	{
		head = p;
		tail = p;
	}
	else
	{
		p->next = head;
		head->prev = p;
		head = p;
	}
}

void List::tailPush(int a)
{
	Node *p = new Node(a);
	if (isEmpty())
	{
		head = p;
		tail = p;
	}
	else
	{
		p->prev = tail;
		tail->next = p;
		tail = p;
	}
}

int List::headPop()
{
	Node *p;
	int data;
	if (isEmpty())
	{
		cout << "No Data\n";
	}
	else
	{
		if (head == tail)
		{
			tail = 0;
			delete head;
		}
		else
		{
			p = head->next;
			data = head->info;
			delete head;
			head = p;
		}
	}
	return data;
}

int List::tailPop()
{
	Node *p;
	int data;
	if (isEmpty())
	{
		cout << "No Data\n";
	}
	else
	{
		if (head == tail)
		{
			head = 0;
			delete tail;
		}
		else
		{
			p = tail->prev;
			data = tail->info;
			delete tail;
			tail = p;
			tail->next = 0;
		}
	}
	return data;
}

void List::deleteNode(int a)
{
	Node *q;

	if (isEmpty())
	{
		cout << "Empty Data" << endl;
	}
	else if (isInList(a))
	{
		if (a == head->info)
		{headPop();}
		else if (a != head->info || a != tail->info)
		{
			for (Node *p = head; p != NULL; p = p->next)
			{
				if (a == p->next->info)
				{
					q = p->next;
					p->next = q->next;
					delete q;
					break;
				}
			}
		}
		else if (a == tail->info)
		{
			tailPop();
		}
	}
}

bool List::isInList(int a)
{
	for (Node *p = head; p != NULL; p = p->next)
	{
		if (a == p->info)
		{
			return true;
		}
	}
	return false;
}

void List::swap(int *variable1, int *variable2)
{
	int temp = *variable1;
	*variable1 = *variable2;
	*variable2 = temp;
}

void List::sort()
{
	for (Node *p = head; p != NULL; p = p->next)
	{
		for (Node *q = p->next; q != NULL; q = q->next)
		{
			if (q->info<p->info)
			{
				swap(&p->info, &q->info);
			}
		}
	}
}

void List::unique()
{
	for (Node *p = head; p != tail; p = p->next)
	{
		for (Node *q = p->next; q != tail; q = q->next)
		{
			if (p->info == q->info)
			{
				Node *r = q->prev;
				r->next = q->next;
				q->next->prev = r;
				delete q;
				q = r;
			}
		}
	}
	//for (Node *p = head; p != tail; p = p->next)

	Node *p=tail->prev;
		if(p->info == tail->info)
		{
			tailPop();
			//deleteNode(tail->info);
		}
	
}